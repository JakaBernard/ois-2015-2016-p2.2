function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf('http://sandbox.lavbic.net/teaching/OIS/gradivo/') > -1;
  if (jeSmesko) {
    sporocilo = sporocilo.replace(/\</g, '&lt;').replace(/\>/g, '&gt;').replace('&lt;img', '<img').replace('png\' /&gt;', 'png\' />');
    return $('<div style="font-weight: bold"></div>').html(sporocilo);
  } else {
    return $('<div style="font-weight: bold;"></div>').text(sporocilo);
  }
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') { //ali je ukaz
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else { //ali je navadno sporocilo
    sporocilo = filtrirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo); //crknio, ker je bralo direkt iz teksta --> damo trenutni kanal
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();
var trenutniVzdevek = "", trenutniKanal = "";

var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki){ //strezniku smo povedali, da nam root stvari streze iz mape public --> zato smo dal v mapo public   streznik.js vrstica 39 (var streznik = ...     pa to)
  vulgarneBesede = podatki.split('\r\n'); //splitamo po new line characterju
}) 

function filtrirajVulgarneBesede(vhod) { //resitev z regularnimi izrazi
  for(var i in vulgarneBesede) {
    vhod = vhod.replace(new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi'), function(){ //RegExp = regularni izraz, \\ zato, da escapamo string pa to.   'gi' global-da ne neha, ko nekej najde,   case insensitive, da je vseeno al so male al velke crke
      var zamenjave = "";                                                                //drugi parameter je s cim zamenjamo
      for(var j = 0; j < vulgarneBesede[i].length; j++) {
        zamenjave = zamenjave +"*";
      }
      return zamenjave;
    }) 
  }                                                                              
  return vhod;
}

$(document).ready(function() { //pocakamo, da se stvar nalozi
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) { //cakamo na odgovor streznika, ko zahtevamo spremembo vzdevka
    var sporocilo;                                          //ko pridemo na kanal nam avtomatsko doloci vzdevek
    if (rezultat.uspesno) { //ce se je dalo spremenit
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + ' @ ' + trenutniKanal); //tudi ko spremenimo vzdevek, se zamenja napis na vrhu (Vzdevek @ Kanal)
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) { //poslusamo na socketu pridruzitevOdgovor --> ko dobimo odgovor na zahtevo o pridruzitvi kanalu
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + ' @ ' + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {  //cakamo, da streznik posreduje sporocilo drugih uporabnikov
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) { //da je seznam kanalom ves cas ažuren
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  socket.on('uporabniki', function(uporabniki) {  //da je seznam uporabnikov ažuren
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
    }
  });

  setInterval(function() {  //to preverja na 1 sekundo (kanale in uporabnike)
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal}); //tud tuki je bralo derekt iz stringa --> podamo ime kanala, da ni več krize
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});

function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.replace(smesko,
      "<img src='http://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}
